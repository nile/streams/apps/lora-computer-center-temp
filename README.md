# LoRa Computer Center Temp

Computer Center temperature sensors are used to monitor temperature in the data center
For detailed information about these sensors, refer to
the [CayenneLPP documentation](https://docs.mydevices.com/docs/lorawan/cayenne-lpp/).

## Overview

### Owners & Administrators

| Title       | Details                                                                                                                    |
|-------------|----------------------------------------------------------------------------------------------------------------------------|
| **Section** | [IT-FA-HCT](https://phonebook.cern.ch/search?q=IT-FA-HCT)                                                                  |
| **E-group** | [lora-it-computer-center-temp-admins](https://groups-portal.web.cern.ch/group/lora-it-computer-center-temp-admins/details) |
| **People**  | [Alexandre Putzu](https://phonebook.cern.ch/search?q=Alexandre+Putzu)                                                      |

### Kafka Topics

| Environment    | Topic Name                                                                                                                          |
|----------------|-------------------------------------------------------------------------------------------------------------------------------------|
| **Production** | [lora-computer-center-temp](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-computer-center-temp)                 |
| **Production** | [lora-computer-center-temp-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-computer-center-temp-decoded) |
| **QA**         | [lora-computer-center-temp](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-computer-center-temp)                 |
| **QA**         | [lora-computer-center-temp-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-computer-center-temp-decoded) |

### Configuration

| Title                        | Details                                                                                                            |
|------------------------------|--------------------------------------------------------------------------------------------------------------------|
| **Application Name**         | lora-IT-computer-center-temp                                                                                       |
| **Configuration Repository** | [app-configs/lora-computer-center-temp](https://gitlab.cern.ch/nile/streams/app-configs/lora-computer-center-temp) |

### Technologies

- LoRa